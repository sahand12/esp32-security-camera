#include <EEPROM.h>
#include <Adafruit_NeoPixel.h>
#include <Keypad.h>
#include "driver/timer.h"
#include "esp_camera.h" // esp_camera_fb_get
#include "Arduino.h"
#include "FS.h"                // SD Card ESP32
#include "SD_MMC.h"            // SD Card ESP32
#include "soc/soc.h"           // Disable brownour problems
#include "soc/rtc_cntl_reg.h"  // Disable brownour problems
#include "Wire.h"
#include "I2CKeyPad.h"
#include <stdio.h>

#include "Adafruit_SSD1306.h"
#include <Adafruit_GFX.h>

// SSD1306 display
#define OLED_ADDR   0x3C
#define OLED_RESET     -1
#define SCREEN_ADDRESS 0x3C
Adafruit_SSD1306 display(128, 64, &Wire, OLED_RESET);

#define I2C_SDA 16
#define I2C_SCL 1
#define MOVEMENT_SENSOR_PIN 3
//#define LED_PIN 16

#define EEPROM_SIZE 3

#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

int pictureNumber = 0;


const int len_key = 5;
char master_key[len_key] = {'1', '8', '5', '7', '1'};
char attempt_key[len_key];
int z = 0;
int locked = 0;
int intrusion;
bool waitforkey = false;
hw_timer_t * timer = NULL;
bool timerRunOut = false;
String state = "GREET"; 


const uint8_t KEYPAD_ADDRESS = 0x20;
I2CKeyPad keyPad(KEYPAD_ADDRESS);
char keys[] = "123A456B789C*0#DNF";  // N = Nokey, F = Fail
char key = '\0'; 

void IRAM_ATTR TimerRunOut() {
  timerRunOut = true;
  state = "TIMERRUNOUT"; 
  if (timer) {
    timerEnd(timer);
    timer = NULL;
  }
}

void display_printf(const char* msg, int font_size, int delayTime) {
  display.clearDisplay();
  display.setTextSize(font_size);      // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setCursor(0, 0);     // Start at top-left corner
  display.println(msg);
  display.display();
  if(delayTime!=0)delay(delayTime * 1000);
}

char nStars[7] = " _____";

void show_reg_key() {
  if (z > 6)return;
  nStars[z] = '*'; 
  //sprintf(&nStars, '*');
  const char* nStarsPtr = nStars;
  display_printf(nStarsPtr, 2, 0);
}



void setup() {
  //Serial.begin(115200);
  Wire.begin(I2C_SDA, I2C_SCL);
  Wire.setClock(400000);
  // SSD1306 display
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    display_printf("SSD1306 allocation failed", 1, 0);
    for (;;); // Don't proceed, loop forever
  }
  display_printf("hellloo \nma bitches!", 1, 3);

  //pinMode(LED_PIN, OUTPUT); // Blinking onboard LED
  //digitalWrite(LED_PIN, LOW);
  pinMode(MOVEMENT_SENSOR_PIN, INPUT); // the input of the IR sensor IO3
  EEPROM.begin(EEPROM_SIZE);
  intrusion = EEPROM.read(0);
  display_printf("device init... EEPROM at 0 (intrusion register) has value: ", 1, 2);
  display_printf((char*)&intrusion, 3, 2);
  //Serial.println(intrusion); 

  // keypad setup
  if (keyPad.begin() == false)
  {
    display_printf("\nERROR: cannot communicate to keypad.\nPlease reboot.\n", 1, 0);
    while (1);
  }
  keyPad.setKeyPadMode(I2C_KEYPAD_4x3);
  keyPad.loadKeyMap(keys);

  // camera setup
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM; // = -1?
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
  if (psramFound()) {
    config.frame_size = FRAMESIZE_UXGA; // FRAMESIZE_ + QVGA|CIF|VGA|SVGA|XGA|SXGA|UXGA
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }
  // Init Camera
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    display_printf("Camera init failed with error", 1, 2);
    display_printf((char*)&err, 1, 2);
    while (1);
  }

  // init SD card
  display_printf("Starting SD Card", 1, 1);
  if (!SD_MMC.begin()) {
    display_printf("SD Card Mount Failed", 1, 2);
    while (1);
  }
  uint8_t cardType = SD_MMC.cardType();
  if (cardType == CARD_NONE) {
    display_printf("No SD Card attached", 1, 0);
    while (1);
  }

}

void loop() {
  if(state == "GREET"){
      display_printf("press * to activate...", 1, 0);
      state = "WAIT2AKTIVATE"; 
  }
  else if(state == "WAIT2AKTIVATE"){
      key = '\0';
      uint8_t idx = keyPad.getKey();
      if (keys[idx] != 'N' & keys[idx] != 'F') {
        key = keys[idx];
        delay(400);
        if (key == '*') {
          display_printf("activation in 15 seconds... \n", 1, 0);
          z = 0;
          locked = 1;
          delay(15000); // 15 sec time before activation
          display_printf("system activated", 1, 0);
          state = "WAIT4MOVEMENT"; 
        }
      }
  }
  else if(state == "WAIT4MOVEMENT"){
      if (digitalRead(MOVEMENT_SENSOR_PIN) == 1) {
        intrusion++;
        waitforkey = true;
        EEPROM.write(0, EEPROM.read(0) + 1);
        EEPROM.commit();
        display_printf("movement detected! \n", 1, 0);
        takeImage();
        //digitalWrite(LED_PIN, HIGH);
        timer = timerBegin(0, 80, true);
        timerAttachInterrupt(timer, & TimerRunOut, true);
        timerAlarmWrite(timer, 15 * 1000 * 1000, true); // 15 seconds to unlock
        timerAlarmEnable(timer);
        display_printf("15 seconds to enter the pin code...", 1, 0);
        state = "WAIT4KEY"; 
        }
  }
   else if(state == "WAIT4KEY"){
      key = '\0';
      uint8_t idx = keyPad.getKey();
      if (keys[idx] != 'N' & keys[idx] != 'F') {
        key = keys[idx];
        delay(400);
      }
      if (key) {
        switch (key) {
          case '#':
            delay(100); // added debounce
            checkKEY();
            key = '\0';
            break;
          default:
            attempt_key[z] = key;
            z++;
            show_reg_key();
            key = '\0';
         }
        }
   } 
   else if(state == "TIMERRUNOUT"){
        EEPROM.write(0, EEPROM.read(0) + 1);
        EEPROM.commit();
        display_printf("15 seconds ran out and password not entered, intrusion registered!", 2, 1);
        state = "WAIT4KEY"; 
    }
}

void takeImage() {
  // Take Picture with Camera
  camera_fb_t * fb = NULL;
  fb = NULL;
  fb = esp_camera_fb_get();
  if (!fb) {
    display_printf("Camera capture failed",1,1);
    return;
  }
  pictureNumber = EEPROM.read(1) + 1;
  if (pictureNumber == 256) {
    pictureNumber = 0;
  }
  // Path where new picture will be saved in SD Card
  String path = "/picture" + String(pictureNumber) + ".jpg";

  fs::FS &fs = SD_MMC;
  //display_printf("Picture file name: %s\n", path.c_str());

  File file = fs.open(path.c_str(), FILE_WRITE);
  if (!file) {
    //display_printf("Failed to open file in writing mode");
  }
  else {
    file.write(fb->buf, fb->len); // payload (image), payload length
    display_printf("Saved file to path: %s\n", 1, 1);
    //display_printf(path.c_str(),1,1);
    EEPROM.write(1, pictureNumber);
    EEPROM.commit();
  }
  file.close();
  esp_camera_fb_return(fb);
}

void checkKEY()
{
  int correct = 0;
  int i;
  for (i = 0; i < len_key; i++) {
    if (attempt_key[i] == master_key[i]) {
      correct++;
    }
  }
  if (correct == len_key && z == len_key) {
    if (EEPROM.read(0) != 1 | timerRunOut == true) {
      //blinkNTimes(4);
      display_printf("intrusion has been detected!!!!! \n", 2, 5);
    }
    //digitalWrite(LED_PIN, LOW);
    intrusion = 0;
    EEPROM.write(0, intrusion);
    EEPROM.commit();
    z = 0;
    if (timer) {
      timerEnd(timer);
      timer = NULL;
    }
    display_printf("unlocked \n", 1, 2);
    state = "GREET"; 
  }
  else
  {
    display_printf("wrong password!", 1, 1);
    z = 0;
  }
  for (int zz = 0; zz < len_key; zz++) {
    attempt_key[zz] = 0;
    nStars[zz+1] = '_'; 
  }
}
