# esp32 Security Camera 

This is an offline security camera based on the esp32 cam module. 
The system has a keypad which is used to activate the system by pressing the "*" key. Once activated, it waits for a signal from the motion sensor. When the system detects movement, it takes a photo and stores it on the SD card on the cam module, starts a countdown and prompts the user to enter the password on the keypad via the LCD. If the password is not entered in time, the system detects an intrusion, which is stored in the EEPROM memory. The system will continue to display an intrusion message until the correct password is entered, even after a restart.

![](photo_5942999741749116523_y.jpg)
